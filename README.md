`tmt` doesn't have fixed directory layout. The only fixed
dir is `.fmf` (which stands for Flexible Metadata Format)
and it contains only `version` file.

```sh
$ ls .fm
version
$ cat .fmf/version
1
```

### `tmt` test discovery

`tmt` walks through the directory tree looking for `.fmf`
files that describe what to run.


### create `.fmf` files

`tmt` has helper to create `.fmf` with test description
together with a sample shell test.
```sh
$ tmt tests create test01
Test template (shell or beakerlib): shell
Test directory '/data/s/gitlab-ai-gateway/xxx/test01' created.
Test metadata '/data/s/gitlab-ai-gateway/xxx/test01/main.fmf' created.
Test script '/data/s/gitlab-ai-gateway/xxx/test01/test.sh' created.
```

Let's see them.
```sh
$ cat test01/test.sh
#!/bin/sh -eux

tmp=$(mktemp)
tmt --help > "$tmp"
grep -C3 'Test Management Tool' "$tmp"
rm "$tmp"
```

So, the `test.sh` creates temp file, outputs help to it,
and then checks that it contains specific line.

```sh
$ cat test01/main.fmf
summary: Concise summary describing what the test does
test: ./test.sh
```

### `tmt run`

```sh
$ tmt run
/var/tmp/tmt/run-001

/default/plan
    discover
        how: fmf
        directory: /data/s/gitlab-ai-gateway/xxx
        summary: 1 test selected
    provision
        queued provision.provision task #1: default-0

        provision.provision task #1: default-0
        how: virtual
        memory: 2048 MB
        disk: 40 GB
        progress: downloading...
^C^C    finish
        warn: Nothing to finish, no guests provisioned.

Aborted!
```

Well, it tries to create some Virtual Machine with 2Gb
memory and 40Gb disk, but I have limited resources, so I
aborted it to run the test in a container.

```sh
$ tmt run provision --how container
/var/tmp/tmt/run-002

/default/plan
    provision
        queued provision.provision task #1: default-0
        
        provision.provision task #1: default-0
        how: container
        multihost name: default-0
        arch: x86_64
        distro: Fedora Linux 40 (Container Image)
    
        summary: 1 guest provisioned
```

Well, it created container, but didn't execute anything.
Need to tell it to run `--all` steps.

```sh
$ tmt run --all provision --how container
/var/tmp/tmt/run-003

/default/plan
    discover
        how: fmf
        directory: /data/s/gitlab-ai-gateway/xxx
        summary: 1 test selected
    provision
        queued provision.provision task #1: default-0
        
        provision.provision task #1: default-0
        how: container
        multihost name: default-0
        arch: x86_64
        distro: Fedora Linux 40 (Container Image)
    
        summary: 1 guest provisioned
    prepare
        queued push task #1: push to default-0
        
        push task #1: push to default-0
    
        queued prepare task #1: requires on default-0
        
        prepare task #1: requires on default-0
        how: install
        summary: Install required packages
        name: requires
        where: default-0
        package: /usr/bin/flock
    
        queued pull task #1: pull from default-0
        
        pull task #1: pull from default-0
    
        summary: 1 preparation applied
    execute
        queued execute task #1: default-0 on default-0
        
        execute task #1: default-0 on default-0
        how: tmt
        progress:              
    
        summary: 1 test executed
    report
        how: display
        summary: 1 error
    finish
    
        container: stopped
        container: removed
        container: network removed
        summary: 0 tasks completed

total: 1 error
```
So the steps are `discover`, `provision`, `prepare`,
`execute`, `report`, `finish`.

1 error, but where? A lot of not-so-useful info, and the
most important is missing - which test and why.

It also decided that some `/usr/bin/flock` package is
required. I have no idea what it is.

Increasinng verbosity is the only way to read test
results.

```sh
$ tmt run -vvv --all provision --how container
/var/tmp/tmt/run-022
Found 1 plan.

/default/plan
    discover
        how: fmf
        order: 50
        directory: /data/s/gitlab-ai-gateway/xxx
        hash: 8769784
        summary: 1 test selected
            /test01
    provision
        queued provision.provision task #1: default-0

        provision.provision task #1: default-0
        how: container
        order: 50
        primary address: tmt-022-xappmjuv
        topology address: tmt-022-xappmjuv
        Check for container image 'fedora'.
        name: tmt-022-xappmjuv
        Create network 'tmt-022-network'.
        multihost name: default-0
        arch: x86_64
        distro: Fedora Linux 40 (Container Image)
        kernel: 6.9.5-200.fc40.x86_64
        package manager: dnf
        selinux: yes
        is superuser: yes
    
        summary: 1 guest provisioned
    prepare
        queued push task #1: push to default-0
        
        push task #1: push to default-0
    
        queued prepare task #1: requires on default-0
        
        prepare task #1: requires on default-0
        how: install
        summary: Install required packages
        name: requires
        order: 70
        where: default-0
        package: 1 package requested
            /usr/bin/flock
            cmd: rpm -q --whatprovides /usr/bin/flock || dnf install -y  /usr/bin/flock
            out: util-linux-core-2.40.1-1.fc40.x86_64
    
        queued pull task #1: pull from default-0
        
        pull task #1: pull from default-0
    
        summary: 1 preparation applied
    execute
        queued execute task #1: default-0 on default-0
        
        execute task #1: default-0 on default-0
        how: tmt
        order: 50
        exit-first: false
            test: Test `tmt --help` output
                cmd: ./test.sh
                out: ++ mktemp
                out: + tmp=/tmp/tmp.XjzAGdJDVh
                out: + tmt --help
                out: ./test.sh: line 4: tmt: command not found
                00:00:00 errr /test01 (on default-0) [1/1]

    
        summary: 1 test executed
    report
        how: display
        order: 50
            errr /test01
                output.txt: /var/tmp/tmt/run-022/default/plan/execute/data/guest/default-0/test01-1/output.txt
                content:
                    ++ mktemp
                    + tmp=/tmp/tmp.XjzAGdJDVh
                    + tmt --help
                    ./test.sh: line 4: tmt: command not found
        summary: 1 error
    finish
    
        container: stopped
        container: removed
        Remove network 'tmt-022-network'.
        container: network removed
    Prune '/default/plan' plan workdir '/var/tmp/tmt/run-022/default/plan'.
        summary: 0 tasks completed

total: 1 error                                                                                          /20,8s
```

Took 20s.

### installing required packages

Let's fix the test by installing `tmt` package.

```diff
--- a/test01/main.fmf
+++ b/test01/main.fmf
@@ -1,2 +1,3 @@
 summary: Test `tmt --help` output
 test: ./test.sh
+require: tmt
```

Be careful to spell `require:` correctly. `tmt` doesn't
fail if `.fmf` file contains invalid keys
(https://github.com/teemtee/tmt/issues/3063).

Run one more time to see that the test is fixed.

```sh
$ tmt run --all provision --how container
...
total: 1 test passed
```


### running tests in a virtual machine

`tmt` runs tests in a vM by default.

```sh
$ tmt run
```

`--all` flag is not necessary, because we did not specify
any steps separately.

```sh
/var/tmp/tmt/run-034

/default/plan
    discover
        how: fmf
        directory: /data/s/gitlab-ai-gateway/xxx
        summary: 1 test selected
    provision
        queued provision.provision task #1: default-0
        
        provision.provision task #1: default-0
        how: virtual
        memory: 2048 MB
        disk: 40 GB
        progress: booting...
        multihost name: default-0
        arch: x86_64
        distro: Fedora Linux 40 (Cloud Edition)
    
        summary: 1 guest provisioned
    prepare
        queued push task #1: push to default-0
        
        push task #1: push to default-0
    
        queued prepare task #1: requires on default-0
        
        prepare task #1: requires on default-0
        how: install
        summary: Install required packages
        name: requires
        where: default-0
        package: /usr/bin/flock and tmt
    
        queued pull task #1: pull from default-0
        
        pull task #1: pull from default-0
    
        summary: 1 preparation applied
    execute
        queued execute task #1: default-0 on default-0
        
        execute task #1: default-0 on default-0
        how: tmt
        progress:              
    
        summary: 1 test executed
    report
        how: display
        summary: 1 test passed
    finish
    
        guest: stopped
        guest: removed
        summary: 0 tasks completed

total: 1 test passed                                /2m15,1s
```

Took 2m15s.


### testing container build tools

Running tests in a container helps to keep the operating
system clean from dependencies and test artifacts. But if
you want to test tools that work with containers, running
containers in container needs a lot of shenanigans. Using
VM for running tests solves these problems.

I want to test that `docker` and `podman` build commands
use Dockerfile from the context directory, and that
`kaniko` follows the behavior.

Let's create test that runs `docker --version`.

```sh
$ cat test02/main.fmf
summary: Test `docker` Dockerfile locations
test: docker --version
require: docker
```

`tmt run` should report `total: 2 tests passed`.

Now modify test to run `docker build` and give it two
`Dockerfile` at different levels which contain different
`level=` environment variable.

```sh
├── Dockerfile
├── level1
│   └── Dockerfile
└── main.fmf

$ cat Dockerfile
FROM scratch

ENV level="zero"

$ cat level1/Dockerfile
FROM scratch

ENV level="first"
```


### testing with `testscript`

`testscript` is a Go testing tool that runs .txtar files.
.txtar contains commands, expectations and contents for
additionnal files, all in a single file.

Some `testfile` tutorials:

* https://bitfieldconsulting.com/posts/tag/testscript
* https://fosdem.org/2024/schedule/event/fosdem-2024-1802-testing-go-command-line-programs-with-go-internal-testscript-/

Install `testscript` utility first.

TBD

